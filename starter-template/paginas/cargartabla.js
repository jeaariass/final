var tabla=[
    

];

window.onload=cargarEventos;


function cargarEventos(){
document.getElementById("mostrar-tabla").addEventListener("click",mostrarTabla,false);
document.getElementById("nuevo-evento").addEventListener("submit",nuevoEvento,false);
}

function mostrarTabla(){
    var cuerpoTabla=document.getElementById("eventos-tabla");
    var tablallena="";
    
    for(let i=0;i<tabla.length;i++){
        tablallena+="<tr><td>"+tabla[i].mes+"</td><td>"+tabla[i].dia+"</td></tr>"+tabla[i].evento+"</td></tr>";

    }
    cuerpoTabla.innerHTML=tablallena;
}
function nuevoEvento(event){
    event.preventDefault();
    var eventoIntroducidoPorUsuario=document.getElementById("cualEvento").value
    var mesIntroducidoPorUsuario=document.getElementById("mes").value
    var diaIntroducidoPorUsuario=document.getElementById("dia").value

    var nuevoEvento={ mes:mesIntroducidoPorUsuario, dia:diaIntroducidoPorUsuario ,evento :eventoIntroducidoPorUsuario };
    tabla.push(nuevoEvento);
    alert("El evento ha sido agregado con éxito");
    ordenartabla();
}
function ordenartabla(){
 
        tabla.sort(function (a, b) {
            if (a.mes > b.mes) {
              return 1;
            }
            if (a.mes < b.mes) {
              return -1;
            }
            // a must be equal to b
            return 0;
          });
          
guardarlocal();
 
}
function guardarlocal(){

    localStorage.setItem("elementos" , JSON.stringify( tabla ));
}
function mostrarLocal(){

    var local=  JSON.parse(localStorage.getItem( "elementos" ));
    tabla=local;
    mostrarTabla();
}


